import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { AppComponent } from './app.component';
import { RepoSelectComponent } from './views/repo-select/repo-select.component';
import { ContributorVisualisationComponent } from './views/contributor-visualisation/contributor-visualisation.component';
import { AppRoutingModule } from './modules/app-routing.module';
import { LoadingButtonComponent } from './components/loading-button/loading-button.component';

@NgModule({
  declarations: [
    AppComponent,
    RepoSelectComponent,
    ContributorVisualisationComponent,
    LoadingButtonComponent
  ],
  imports: [
    BrowserModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
