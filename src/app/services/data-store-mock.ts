import { Subject } from 'rxjs';

export class MockDataStoreService {
  contributors: Subject<any>;

  constructor() {
    this.contributors = new Subject();
  }
}
