import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { LoadingButtonComponent } from './loading-button.component';

describe('LoadingButtonComponent', () => {
  let component: LoadingButtonComponent;
  let fixture: ComponentFixture<LoadingButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoadingButtonComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be disabled if disabled is defined', () => {
    const button: HTMLElement = fixture.debugElement.query(By.css('button')).nativeElement;
    const hasClass = () => button.classList.contains('disabled');
    const hasAttribute = () => button.hasAttribute('disabled');

    expect(hasAttribute()).toBeFalsy();
    expect(hasClass()).toBeFalsy();

    component.disabled = true;
    fixture.detectChanges();

    expect(hasAttribute()).toBeTruthy();
    expect(hasClass()).toBeTruthy();
  });

  it('should specify button type', () => {
    const button: HTMLElement = fixture.debugElement.query(By.css('button')).nativeElement;
    const getType = () => button.getAttribute('type');

    expect(getType()).toBe('button');

    component.type = 'submit';
    fixture.detectChanges();

    expect(getType()).toBe('submit');
  });

  it('should display loading overlay', () => {
    const getSpinner = () => fixture.debugElement.query(By.css('.spinner'));

    expect(getSpinner()).toBeFalsy();

    component.loading = true;
    fixture.detectChanges();

    expect(getSpinner()).toBeTruthy();
  });
});
