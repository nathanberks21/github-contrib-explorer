import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { ChartData } from '../interfaces/ChartData';

@Injectable({
  providedIn: 'root'
})
export class DataStoreService {
  public contributors: BehaviorSubject<ChartData[]>;

  constructor() {
    this.contributors = new BehaviorSubject(null);
  }
}
