import { TestBed } from '@angular/core/testing';

import { take } from 'rxjs/operators';

import { DataStoreService } from './data-store.service';

describe('DataStoreService', () => {
  let dataStoreService: DataStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataStoreService]
    });

    dataStoreService = TestBed.get(DataStoreService);
  });

  describe('contributors store', () => {
    it('should start with null', done => {
      dataStoreService.contributors.pipe(take(1)).subscribe(d => {
        expect(d).toBeNull();
        done();
      });
    });

    it('should return the latest value', done => {
      const value = {
        name: 'foo',
        value: 'bar'
      };
      const data = [value];
      dataStoreService.contributors.next(data);
      data.push(value);
      dataStoreService.contributors.next(data);
      dataStoreService.contributors.pipe(take(1)).subscribe(d => {
        expect(d).toEqual(data);
        done();
      });
    });
  });
});
