import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { of, Subject } from 'rxjs';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { DataStoreService } from '../../services/data-store.service';
import { GithubApiService } from '../../services/github-api.service';

import { MockDataStoreService } from '../../services/data-store-mock';
import { MockGithubApiService } from '../../services/github-api-mock';

import { ContributorVisualisationComponent } from './contributor-visualisation.component';

class MockActivatedRoute {
  queryParams: Subject<any>;

  constructor() {
    this.queryParams = new Subject();
  }
}

describe('ContributorVisualisationComponent', () => {
  let component: ContributorVisualisationComponent;
  let fixture: ComponentFixture<ContributorVisualisationComponent>;
  let mockDataStoreService: MockDataStoreService;
  let mockGithubApiService: MockGithubApiService;
  let mockActivatedRoute: MockActivatedRoute;

  beforeEach(async(() => {
    mockDataStoreService = new MockDataStoreService();
    mockGithubApiService = new MockGithubApiService();
    mockActivatedRoute = new MockActivatedRoute();
    TestBed.configureTestingModule({
      imports: [
        NgxChartsModule,
        NoopAnimationsModule,
        RouterTestingModule
      ],
      declarations: [ ContributorVisualisationComponent ],
      providers: [
        { provide: DataStoreService, useValue: mockDataStoreService },
        { provide: GithubApiService, useValue: mockGithubApiService },
        { provide: ActivatedRoute, useValue: mockActivatedRoute }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributorVisualisationComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => mockDataStoreService = mockGithubApiService = mockActivatedRoute = null);

  describe('ngOnInit', () => {
    it('navigates to lookup if missing params', () => {
      const navigateSpy = spyOn((<any>component).router, 'navigate');
      fixture.detectChanges();
      mockDataStoreService.contributors.next(null);
      mockActivatedRoute.queryParams.next({});
      expect(navigateSpy).toHaveBeenCalledWith(['lookup']);
    });

    it('re-fetches contributor data if params are defined but contributors is undefined', () => {
      mockGithubApiService.setMockData(of('foobar'));
      fixture.detectChanges();
      mockDataStoreService.contributors.next(null);
      mockActivatedRoute.queryParams.next({
        user: 'foo',
        repo: 'bar'
      });
      expect(component.user).toEqual('foo');
      expect(component.repo).toEqual('bar');
      expect(component.barChartConfig.results).toEqual('foobar' as any);
      expect(component.pieChartConfig.results).toEqual('foobar' as any);
    });

    it('sets graph data if params and contributors are defined', () => {
      fixture.detectChanges();
      mockDataStoreService.contributors.next('foobar');
      mockActivatedRoute.queryParams.next({
        user: 'foo',
        repo: 'bar'
      });
      expect(component.user).toEqual('foo');
      expect(component.repo).toEqual('bar');
      expect(component.barChartConfig.results).toEqual('foobar' as any);
      expect(component.pieChartConfig.results).toEqual('foobar' as any);
    });
  });

  it('defaults to display a bar chart', () => {
    expect(component.chartType).toEqual('bar');
  });

  it('can toggle between chart types', () => {
    component.setChartType('pie');
    expect(component.chartType).toEqual('pie');
    component.setChartType('bar');
    expect(component.chartType).toEqual('bar');
  });
});
