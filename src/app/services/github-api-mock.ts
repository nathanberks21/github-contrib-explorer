import { Observable } from 'rxjs';

export class MockGithubApiService {
  private mockData: any;

  setMockData(data: Observable<any>) {
    this.mockData = data;
  }

  getContributors(): Observable<any> {
    return this.mockData;
  }
}
