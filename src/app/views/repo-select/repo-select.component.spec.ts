import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { of, throwError } from 'rxjs';
import { delay } from 'rxjs/operators';

import { DataStoreService } from '../../services/data-store.service';
import { GithubApiService } from '../../services/github-api.service';

import { MockDataStoreService } from '../../services/data-store-mock';
import { MockGithubApiService } from '../../services/github-api-mock';

import { LoadingButtonComponent } from '../../components/loading-button/loading-button.component';
import { RepoSelectComponent } from './repo-select.component';

describe('RepoSelectComponent', () => {
  let component: RepoSelectComponent;
  let fixture: ComponentFixture<RepoSelectComponent>;
  let mockDataStoreService: MockDataStoreService;
  let mockGithubApiService: MockGithubApiService;

  beforeEach(async(() => {
    mockDataStoreService = new MockDataStoreService();
    mockGithubApiService = new MockGithubApiService();
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        NoopAnimationsModule,
        RouterTestingModule
      ],
      declarations: [
        LoadingButtonComponent,
        RepoSelectComponent
      ],
      providers: [
        { provide: DataStoreService, useValue: mockDataStoreService },
        { provide: GithubApiService, useValue: mockGithubApiService },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepoSelectComponent);
    component = fixture.componentInstance;
    // Spy on router so routes are provided
    spyOn((<any>component).router, 'navigate');
    fixture.detectChanges();
  });

  afterEach(() => mockDataStoreService = mockGithubApiService = null);

  describe('fetchProjectData', () => {
    it('should push contributors to the dataStore', done => {
      mockGithubApiService.setMockData(of('foo'));
      mockDataStoreService.contributors.subscribe(result => {
        expect(result).toEqual('foo');
        done();
      });
      component.fetchProjectData(null, null);
    });

    it('sets loading state', done => {
      // Adding a delay allows for the loading property to be set
      mockGithubApiService.setMockData(of('foo').pipe(delay(0)));
      mockDataStoreService.contributors.subscribe(result => {
        expect(component.loading).toBeFalsy();
        done();
      });
      component.fetchProjectData(null, null);
      expect(component.loading).toBeTruthy();
    });

    it('sets an error if the call fails', () => {
      mockGithubApiService.setMockData(throwError('foo'));
      component.fetchProjectData(null, null);
      expect(component.error).toBeTruthy();
    });
  });
});
