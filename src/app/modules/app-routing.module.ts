import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RepoSelectComponent } from '../views/repo-select/repo-select.component';
import { ContributorVisualisationComponent } from '../views/contributor-visualisation/contributor-visualisation.component';

const routes: Routes = [
  { path: 'lookup', component: RepoSelectComponent, data: { state: 'lookup' } },
  {
    path: 'contributors',
    component: ContributorVisualisationComponent,
    data: { state: 'contributors' }
  },
  { path: '', redirectTo: '/lookup', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
