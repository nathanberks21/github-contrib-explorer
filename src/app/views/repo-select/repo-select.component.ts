import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { first } from 'rxjs/operators';

import { DataStoreService } from '../../services/data-store.service';
import { GithubApiService } from '../../services/github-api.service';

@Component({
  selector: 'app-repo-select',
  templateUrl: './repo-select.component.html',
  styleUrls: ['./repo-select.component.css']
})
export class RepoSelectComponent {
  repo: string;
  user: string;
  error: boolean;
  loading: boolean;

  constructor(
    private router: Router,
    private dataStore: DataStoreService,
    private githubApi: GithubApiService
  ) {}

  fetchProjectData(user: string, repo: string): void {
    this.loading = true;
    this.error = false;
    this.githubApi
      .getContributors(user, repo)
      .pipe(first())
      .subscribe(
        contributors => {
          this.loading = false;
          this.dataStore.contributors.next(contributors);
          this.router.navigate(['contributors'], { queryParams: { user, repo } });
        },
        () => {
          this.loading = false;
          this.error = true;
        }
      );
  }
}
