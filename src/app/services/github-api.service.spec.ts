import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { take } from 'rxjs/operators';

import { API_URL, GithubApiService } from './github-api.service';

const MOCK_RESPONSE_DATA = [
  {
    login: 'foo',
    contributions: '1',
    foo: 'a'
  },
  {
    login: 'bar',
    contributions: '2',
    foo: 'b'
  },
  {
    login: 'baz',
    contributions: '3',
    foo: 'c'
  }
];

describe('GithubApiService', () => {
  let httpMock: HttpTestingController;
  let githubApiService: GithubApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GithubApiService]
    });
    httpMock = TestBed.get(HttpTestingController);
    githubApiService = TestBed.get(GithubApiService);
  });

  it('should format response data', done => {
    const user = 'foo',
       repo = 'bar';

       githubApiService.getContributors('foo', 'bar')
        .pipe(take(1))
        .subscribe(res => {
          expect(res).toEqual([
            {
              name: 'foo',
              value: '1'
            },
            {
              name: 'bar',
              value: '2'
            },
            {
              name: 'baz',
              value: '3'
            }
          ]);
          done();
        });

    const contributorsRequest = httpMock.expectOne(`${API_URL}/${user}/${repo}/contributors`);
    contributorsRequest.flush(MOCK_RESPONSE_DATA);
  });
});
