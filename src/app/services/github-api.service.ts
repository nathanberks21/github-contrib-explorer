import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GithubResponse } from '../interfaces/GitHubResponse';
import { ChartData } from '../interfaces/ChartData';

export const API_URL = 'https://api.github.com/repos';

@Injectable({
  providedIn: 'root'
})
export class GithubApiService {
  constructor(private http: HttpClient) {}

  getContributors(user, repo): Observable<ChartData[]> {
    return this.http
      .get<GithubResponse[]>(`${API_URL}/${user}/${repo}/contributors`)
      .pipe(map(mapContributors));
  }
}

function mapContributors(contributors): ChartData[] {
  return contributors.map(({ login: name, contributions: value }): ChartData => ({
    name,
    value
  }));
}
