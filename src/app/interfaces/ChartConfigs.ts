import { TemplateRef } from '@angular/core';
import { ChartData } from './ChartData';

export interface BarChartConfig {
  activeEntries?: any[];
  animations?: boolean;
  barPadding?: number;
  customColors?: any;
  gradient?: boolean;
  legend?: boolean;
  legendTitle?: string;
  results?: ChartData[];
  roundDomains?: boolean;
  roundEdges?: boolean;
  scheme?: any;
  schemeType?: 'linear' | 'ordinal';
  showDataLabel?: boolean;
  showGridLines?: boolean;
  showXAxisLabel?: boolean;
  showYAxisLabel?: boolean;
  tooltipDisabled?: boolean;
  tooltipTemplate?: TemplateRef<any>;
  view?: number[];
  xAxis?: boolean;
  xAxisLabel?: string;
  xAxisTickFormatting?: any;
  xAxisTicks?: any[];
  yAxis?: boolean;
  yAxisLabel?: string;
  yAxisTickFormatting?: any;
  yAxisTicks?: any[];
  yScaleMax?: number;
  yScaleMin?: number;
}

export interface PieChartConfig {
  activeEntries?: any[];
  animations?: boolean;
  arcWidth?: number;
  customColors?: any;
  doughnut?: boolean;
  explodeSlices?: boolean;
  gradient?: boolean;
  labelFormatting?: any;
  labels?: boolean;
  legend?: boolean;
  legendTitle?: string;
  maxLabelLength?: number;
  results?: ChartData[];
  scheme?: any;
  tooltipDisabled?: boolean;
  tooltipTemplate?: TemplateRef<any>;
  tooltipText?: boolean;
  trimLabels?: boolean;
  view?: number[];
}
