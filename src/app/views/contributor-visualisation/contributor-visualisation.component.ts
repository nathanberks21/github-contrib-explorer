import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { combineLatest, of, throwError, Observable } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { DataStoreService } from '../../services/data-store.service';
import { GithubApiService } from '../../services/github-api.service';

import { BarChartConfig, PieChartConfig } from '../../interfaces/ChartConfigs';
import { ChartData } from '../../interfaces/ChartData';

@Component({
  selector: 'app-contributor-visualisation',
  templateUrl: './contributor-visualisation.component.html',
  styleUrls: ['./contributor-visualisation.component.css']
})
export class ContributorVisualisationComponent implements OnInit {
  barChartConfig: BarChartConfig = {
    gradient: true,
    results: [],
    roundEdges: false,
    showGridLines: false,
    showXAxisLabel: true,
    showYAxisLabel: true,
    xAxis: true,
    xAxisLabel: 'Username',
    yAxis: true,
    yAxisLabel: 'Contributions'
  };
  pieChartConfig: PieChartConfig = {
    gradient: true,
    labels: true,
    results: []
  };
  repo: string;
  user: string;
  chartType: 'bar' | 'pie' = 'bar';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataStore: DataStoreService,
    private githubApi: GithubApiService
  ) {}

  ngOnInit() {
    this.getContributorsData().subscribe(
      ([params, contributors]) => {
        this.barChartConfig.results = this.pieChartConfig.results = contributors;
        this.user = params.user;
        this.repo = params.repo;
      },
      () => this.router.navigate(['lookup'])
    );
  }

  setChartType(type: 'bar' | 'pie') {
    this.chartType = type;
  }

  private getContributorsData(): Observable<[Params, ChartData[]]> {
    return combineLatest(
      this.route.queryParams,
      this.dataStore.contributors
    ).pipe(
      first(),
      switchMap(([params, contributors]) => {
        /**
         * If no params or contributors are ready throw an error
         * else if params are defined but no contributors attempt to get data
         * else return params and contributors
         */
        if (!contributors && (!params.user || !params.repo)) {
          return throwError('No data');
        } else if (!contributors) {
          return combineLatest(
            of(params),
            this.githubApi.getContributors(params.user, params.repo)
          );
        }
        return combineLatest(of(params), of(contributors));
      })
    );
  }
}
