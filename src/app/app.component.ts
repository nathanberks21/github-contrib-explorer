import { Component } from '@angular/core';
import { viewAnimation } from './util/animations';

@Component({
  selector: 'app-root',
  template: `
    <div [@viewAnimation]="getState(o)">
      <router-outlet #o="outlet"></router-outlet>
    </div>
  `,
  animations: [viewAnimation]
})
export class AppComponent {
  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
}
