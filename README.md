# GitHub Contribution Explorer

This small web app displays the top 30 contributors to a given GitHub repo by searching for a `user` and `repo`.

The app demonstrates form validation, RxJS data handling, animations between routes, and data visualisation using ngx-charts.

## Development server

Run `npm start` to start the dev server.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## CI

GitLab CI is used to generate a `pages` demo view, which can be [viewed here](https://nathanberks21.gitlab.io/github-contrib-explorer/).

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
