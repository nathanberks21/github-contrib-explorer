import {
  trigger,
  animate,
  transition,
  style,
  query
} from '@angular/animations';

const scrollIn = [
  query(':enter, :leave', style({ position: 'fixed', width: '100%', height: '100%' }), { optional: true }),
  query(
    ':leave',
    [style({ top: 0 }), animate('0.3s', style({ top: '-100%' }))],
    { optional: true }
  )
];

const scrollOut = [
  query(':enter, :leave', style({ position: 'fixed', width: '100%', height: '100%' }), { optional: true }),
  query(
    ':leave',
    [style({ top: 0 }), animate('0.3s', style({ top: '100%' }))],
    { optional: true }
  )
];

export const viewAnimation = trigger('viewAnimation', [
  transition('lookup => contributors', scrollIn),
  transition('contributors => lookup', scrollOut)
]);
